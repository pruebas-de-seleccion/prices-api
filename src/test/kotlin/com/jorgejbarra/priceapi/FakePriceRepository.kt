package com.jorgejbarra.priceapi

class FakePriceRepository : PriceRepository {

    private val storage: MutableList<Price> = mutableListOf()
    override fun getPrices(): List<Price> = storage

    fun fakeData(vararg price: Price) {
        this.storage.clear()
        this.storage.addAll(price)
    }
}
