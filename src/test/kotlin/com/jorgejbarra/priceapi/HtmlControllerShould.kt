package com.jorgejbarra.priceapi

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.HttpStatus.OK
import java.time.Instant
import java.time.temporal.ChronoUnit

class HtmlControllerShould {

    private val fakePriceRepository = FakePriceRepository()
    private val htmlController: HtmlController = HtmlController(fakePriceRepository)

    @Test
    fun `return not found (404) when there are not prices for given brand`() {
        // given
        val unknownBrand = 2

        // when
        val response = htmlController.getPrice(unknownBrand, 1, Instant.now())

        // then
        assertThat(response.statusCode).isEqualTo(NOT_FOUND)
    }

    @Test
    fun `return not found (404) when there are not prices for given product`() {
        // given
        val unknownProduct = 2

        // when
        val response = htmlController.getPrice(1, unknownProduct, Instant.now())

        // then
        assertThat(response.statusCode).isEqualTo(NOT_FOUND)
    }

    @Test
    fun `return not found (404) when there are not active prices for given product and brand`() {
        // given

        // when
        val response = htmlController.getPrice(1, 1, Instant.now())

        // then
        assertThat(response.statusCode).isEqualTo(NOT_FOUND)
    }

    @Test
    fun `return expected price when there is only one match`() {
        // given
        val price = aPrice(
            startApplicationDate = Instant.now().minus(20, ChronoUnit.MINUTES),
            endApplicationDate = Instant.now().plus(20, ChronoUnit.MINUTES)
        )
        fakePriceRepository.fakeData(price)

        // when
        val response = htmlController.getPrice(price.brandId, price.productId, Instant.now())
        // then
        assertThat(response.statusCode).isEqualTo(OK)
        assertThat(response.body?.priceAmount).isEqualTo(35.50)
    }

    @Test
    fun `return expected price when time is startDate`() {
        // given
        val price = aPrice(
            Instant.now().minus(20, ChronoUnit.MINUTES),
            Instant.now().plus(20, ChronoUnit.MINUTES),
        )
        fakePriceRepository.fakeData(price)

        // when
        val response = htmlController.getPrice(price.brandId, price.productId, price.startApplicationDate)

        // then
        assertThat(response.statusCode).isEqualTo(OK)
        assertThat(response.body).isEqualTo(price)
    }

    @Test
    fun `return expected price when time is endDate`() {
        // given
        val price = aPrice(
            Instant.now().minus(20, ChronoUnit.MINUTES),
            Instant.now().plus(20, ChronoUnit.MINUTES),
        )
        fakePriceRepository.fakeData(price)

        //when
        val response = htmlController.getPrice(price.brandId, price.productId, price.endApplicationDate)

        // then
        assertThat(response.statusCode).isEqualTo(OK)
        assertThat(response.body).isEqualTo(price)
    }

    @Test
    fun `return price with more priority`() {
        // given
        val price = aPrice(
            Instant.now().minus(20, ChronoUnit.MINUTES),
            Instant.now().plus(20, ChronoUnit.MINUTES),
        )
        val morePriorityPrice = price.copy(priority = 2)

        fakePriceRepository.fakeData(price, morePriorityPrice)

        // when
        val response = htmlController.getPrice(price.brandId, price.productId, Instant.now())

        // then
        assertThat(response.statusCode).isEqualTo(OK)
        assertThat(response.body).isEqualTo(morePriorityPrice)
    }

    private fun aPrice(
        startApplicationDate: Instant,
        endApplicationDate: Instant,
    ): Price {
        return Price(
            id = 1,
            brandId = 1,
            productId = 1,
            startApplicationDate = startApplicationDate,
            endApplicationDate = endApplicationDate,
            priceListId = 1,
            priority = 0,
            priceCurrency = "EUR",
            priceAmount = 35.50
        )
    }
}
