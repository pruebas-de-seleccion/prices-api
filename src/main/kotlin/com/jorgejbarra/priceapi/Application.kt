package com.jorgejbarra.priceapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PriceApiApplication

fun main(args: Array<String>) {
	runApplication<PriceApiApplication>(*args)
}
