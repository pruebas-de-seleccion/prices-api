package com.jorgejbarra.priceapi

import org.springframework.stereotype.Repository
import java.time.Instant

interface PriceRepository {
    fun getPrices(): List<Price>
}

@Repository
class InMemoryPriceRepository : PriceRepository {
    override fun getPrices(): List<Price> {
        return listOf(
            Price(
                id = 1,
                startApplicationDate = Instant.parse("2020-06-14T00:00:00Z"),
                endApplicationDate = Instant.parse("2020-12-31T23:59:59Z"),
                priceListId = 1,
                priority = 0,
                priceAmount = 35.50
            ),
            Price(
                id = 2,
                startApplicationDate = Instant.parse("2020-06-14T15:00:00Z"),
                endApplicationDate = Instant.parse("2020-06-14T18:30:00Z"),
                priceListId = 2,
                priority = 1,
                priceAmount = 25.45
            ),
            Price(
                id = 3,
                startApplicationDate = Instant.parse("2020-06-15T00:00:00Z"),
                endApplicationDate = Instant.parse("2020-06-15T11:00:00Z"),
                priceListId = 3,
                priority = 1,
                priceAmount = 30.50
            ),
            Price(
                id = 4,
                startApplicationDate = Instant.parse("2020-06-15T16:00:00Z"),
                endApplicationDate = Instant.parse("2020-12-31T23:59:59Z"),
                priceListId = 4,
                priority = 1,
                priceAmount = 38.95
            ),
        )
    }

}
