package com.jorgejbarra.priceapi

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.Instant
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@RestController
class HtmlController(private val pricesRepository: PriceRepository) {

    @GetMapping("/brands/{brandId}/products/{productId}/prices")
    fun getPrice(
        @PathVariable("brandId") brandId: Int,
        @PathVariable("productId") productId: Int,
        @RequestParam("applicationDate") applicationDate: Instant
    ): ResponseEntity<Price> {
        return pricesRepository.getPrices().filter { candidate ->
            candidate.startApplicationDate == applicationDate || candidate.startApplicationDate.isBefore(applicationDate)
        }
            .filter { candidate ->
                candidate.endApplicationDate == applicationDate || candidate.endApplicationDate.isAfter(applicationDate)
            }
            .sortedByDescending { candidate: Price -> candidate.priority }
            .firstOrNull()
            ?.let { activatedPrice ->
                ResponseEntity.ok(activatedPrice)
            }
            ?: ResponseEntity.notFound().build()
    }
}

@Entity(name = "prices")
data class Price(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Int,
    var brandId: Int = 1,
    var productId: Int = 35455,
    var startApplicationDate: Instant,
    var endApplicationDate: Instant,
    var priceListId: Int,
    var priority: Int,
    var priceCurrency: String = "EUR",
    var priceAmount: Double,
)