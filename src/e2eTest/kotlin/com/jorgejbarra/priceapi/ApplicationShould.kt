package com.jorgejbarra.priceapi

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT
import org.springframework.http.HttpStatus
import org.springframework.web.client.RestTemplate
import java.time.Instant

@SpringBootTest(webEnvironment = DEFINED_PORT)
class ApplicationShould {

    @Test
    // Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)
    fun `satisfy test case 1`() {
        // given
        val expectedBrandId = 1
        val expectedProductId = 35455
        val applicationDate = Instant.parse("2020-06-14T10:00:00Z")

        val httpClient = RestTemplate()

        // when
        val response = httpClient.getForEntity(
            "http://localhost:8080/brands/$expectedBrandId/products/$expectedProductId/prices?applicationDate=$applicationDate",
            DatedPrice::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body).isNotNull
        assertThat(response.body?.brandId).isEqualTo(expectedBrandId)
        assertThat(response.body?.productId).isEqualTo(expectedProductId)
        assertThat(response.body?.priceListId).isEqualTo(1)
        assertThat(response.body?.priceAmount).isEqualTo(35.50)

        assertThat(response.body?.startApplicationDate).isEqualTo("2020-06-14T00:00:00Z")
        assertThat(response.body?.endApplicationDate).isEqualTo("2020-12-31T23:59:59Z")
    }

    @Test
    // Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)
    fun `satisfy test case 2`() {
        // given
        val expectedBrandId = 1
        val expectedProductId = 35455
        val applicationDate = Instant.parse("2020-06-14T16:00:00Z")

        val httpClient = RestTemplate()

        // when
        val response = httpClient.getForEntity(
            "http://localhost:8080/brands/$expectedBrandId/products/$expectedProductId/prices?applicationDate=$applicationDate",
            DatedPrice::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body).isNotNull
        assertThat(response.body?.brandId).isEqualTo(expectedBrandId)
        assertThat(response.body?.productId).isEqualTo(expectedProductId)
        assertThat(response.body?.priceListId).isEqualTo(2)
        assertThat(response.body?.priceAmount).isEqualTo(25.45)

        assertThat(response.body?.startApplicationDate).isEqualTo("2020-06-14T15:00:00Z")
        assertThat(response.body?.endApplicationDate).isEqualTo("2020-06-14T18:30:00Z")
    }
}

class DatedPrice(
    var productId: Int,
    var brandId: Int,
    var startApplicationDate: Instant,
    var endApplicationDate: Instant,
    var priceAmount: Double,
    var priceCurrency: String,
    var priceListId: Int
)